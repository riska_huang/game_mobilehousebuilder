﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    private GameObject perspectiveCamera, ortoCamera;

    private void Start()
    {
        perspectiveCamera = transform.GetChild(0).gameObject;
        ortoCamera = transform.GetChild(1).gameObject;
    }

    /// <summary>
    /// If the current camera is perspective camera, change it to ortographic camera and vice versa
    /// </summary>
    public void SwitchCamera()
    {
        if (perspectiveCamera.activeSelf)
        {
            perspectiveCamera.SetActive(false);
            ortoCamera.SetActive(true);
            StaticClass.mainCamera = ortoCamera.GetComponent<Camera>();
        }
        else if (!perspectiveCamera.activeSelf)
        {
            perspectiveCamera.SetActive(true);
            ortoCamera.SetActive(false);
            StaticClass.mainCamera = perspectiveCamera.GetComponent<Camera>();
        }
    }
}
