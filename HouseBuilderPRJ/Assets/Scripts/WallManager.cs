﻿using UnityEngine;

public class WallManager : MonoBehaviour
{
    private GameObject gb;

    //Wall Gameobject
    private GameObject wallXMin, wallXMax, wallZMin, wallZMax, floorGB, objectUI, wallUI;

    /// <summary>
    /// Initialize the variable
    /// </summary>
    private void Awake()
    {
        gb = this.gameObject;

        wallXMin = gb.transform.GetChild(0).gameObject;
        wallXMax = gb.transform.GetChild(1).gameObject;
        wallZMin = gb.transform.GetChild(2).gameObject;
        wallZMax = gb.transform.GetChild(3).gameObject;

        floorGB = gb.transform.GetChild(4).gameObject;
        objectUI = gb.transform.GetChild(5).gameObject;
        wallUI = gb.transform.GetChild(6).gameObject;
    }

    #region Scaling Wall
    #region Scaling API
    /// <summary>
    /// Moving individual wall by certain value
    /// </summary>
    /// <param name="wallGB"></param>
    /// <param name="axis"></param>
    /// <param name="moveValue"></param>
    private void MoveWall(GameObject wallGB, string axis, float moveValue)
    {
        Vector3 tempPos = wallGB.transform.localPosition;
        switch (axis)
        {
            case "z":
                tempPos.z += moveValue;
                break;
            case "x":
                tempPos.x += moveValue;
                break;
        }
        wallGB.transform.localPosition = tempPos;
    }

    /// <summary>
    /// Scaling individual wall by certain value
    /// </summary>
    /// <param name="wallGB"></param>
    /// <param name="axis"></param>
    /// <param name="scaleValue"></param>
    private void ScaleWall(GameObject wallGB, string axis, float scaleValue)
    {
        Vector3 tempScale = wallGB.transform.localScale;
        switch (axis)
        {
            case "z":
                tempScale.z += scaleValue;
                break;
            case "x":
                tempScale.x += scaleValue;
                break;
        }
        wallGB.transform.localScale = tempScale;
    }

    /// <summary>
    /// scale the UI by certain value
    /// </summary>
    /// <param name="axis"></param>
    /// <param name="scaleValue"></param>
    private void ScaleUI(string axis, float scaleValue)
    {
        Vector2 tempScale = wallUI.GetComponent<RectTransform>().sizeDelta;
        switch (axis)
        {
            case "x":
                tempScale.x += scaleValue;
                break;
            case "z":
                tempScale.y += scaleValue;
                break;
        }
        wallUI.GetComponent<RectTransform>().sizeDelta = tempScale;
    }

    /// <summary>
    /// get the UI's scale value
    /// </summary>
    /// <param name="axis"></param>
    /// <returns></returns>
    private float GetUIScale(string axis)
    {
        Vector2 tempScale = wallUI.GetComponent<RectTransform>().sizeDelta;
        switch (axis)
        {
            case "x":
                return tempScale.x;
                break;
            case "z":
                return tempScale.y;
                break;
        }
        return 0;
    }
    #endregion

    #region Scaling individual wall
    public void ScaleXMin(bool wallOutside)
    {
        if (wallOutside)
        {
            //move -z axis
            MoveWall(wallXMin,"z", -1);

            //zmin wall
            MoveWall(wallZMin, "z", -0.5f);
            ScaleWall(wallZMin, "z", 1);

            //zmax wall
            MoveWall(wallZMax, "z", -0.5f);
            ScaleWall(wallZMax, "z", 1);

            //floor
            MoveWall(floorGB, "z", -0.5f);
            ScaleWall(floorGB, "z", 1);

            //ui
            ScaleUI("z", 1);
            MoveWall(wallUI, "z", -0.5f);
        }
        else if (GetUIScale("z") > 1.6f)
        {
            //move -z axis
            MoveWall(wallXMin, "z", 1);

            //zmin wall
            MoveWall(wallZMin, "z", 0.5f);
            ScaleWall(wallZMin, "z", -1);

            //zmax wall
            MoveWall(wallZMax, "z", 0.5f);
            ScaleWall(wallZMax, "z", -1f);

            //floor
            MoveWall(floorGB, "z", 0.5f);
            ScaleWall(floorGB, "z", -1);

            //ui
            ScaleUI("z", -1);
            MoveWall(wallUI, "z", 0.5f);
        }
    }

    public void ScaleXMax(bool wallOutside)
    {
        if (wallOutside && GetUIScale("z") > 1.6f)
        {
            //move -z axis
            MoveWall(wallXMax, "z", -1);

            //zmin wall
            MoveWall(wallZMin, "z", -0.5f);
            ScaleWall(wallZMin, "z", -1);

            //zmax wall
            MoveWall(wallZMax, "z", -0.5f);
            ScaleWall(wallZMax, "z", -1);

            //floor
            MoveWall(floorGB, "z", -0.5f);
            ScaleWall(floorGB, "z", -1);

            //ui
            ScaleUI("z", -1);
            MoveWall(wallUI, "z", -0.5f);
        }
        else if(!wallOutside)
        {
            //move -z axis
            MoveWall(wallXMax, "z", 1);

            //zmin wall
            MoveWall(wallZMin, "z", 0.5f);
            ScaleWall(wallZMin, "z", 1);

            //zmax wall
            MoveWall(wallZMax, "z", 0.5f);
            ScaleWall(wallZMax, "z", 1);

            //floor
            MoveWall(floorGB, "z", 0.5f);
            ScaleWall(floorGB, "z", 1);

            //ui
            ScaleUI("z", 1);
            MoveWall(wallUI, "z", 0.5f);
        }
    }

    public void ScaleZMin(bool wallOutside)
    {
        if (wallOutside && GetUIScale("x") > 1.6f)
        {
            MoveWall(wallZMin, "x", 1);

            MoveWall(wallXMin, "x", 0.5f);
            ScaleWall(wallXMin, "x", -1);

            MoveWall(wallXMax, "x", 0.5f);
            ScaleWall(wallXMax, "x", -1);

            //floor
            MoveWall(floorGB, "x", 0.5f);
            ScaleWall(floorGB, "x", -1);

            //ui
            ScaleUI("x", -1);
            MoveWall(wallUI, "x", 0.5f);
        }
        else if (!wallOutside)
        {
            MoveWall(wallZMin, "x", -1);

            MoveWall(wallXMin, "x", -0.5f);
            ScaleWall(wallXMin, "x", 1);

            MoveWall(wallXMax, "x", -0.5f);
            ScaleWall(wallXMax, "x", 1);

            //floor
            MoveWall(floorGB, "x", -0.5f);
            ScaleWall(floorGB, "x", 1);

            //ui
            ScaleUI("x", 1);
            MoveWall(wallUI, "x", -0.5f);
        }
    }

    public void ScaleZMax(bool wallOutside)
    {
        if (wallOutside)
        {
            MoveWall(wallZMax, "x", 1);

            MoveWall(wallXMin, "x", 0.5f);
            ScaleWall(wallXMin, "x", 1);

            MoveWall(wallXMax, "x", 0.5f);
            ScaleWall(wallXMax, "x", 1);

            //floor
            MoveWall(floorGB, "x", 0.5f);
            ScaleWall(floorGB, "x", 1);

            //ui
            ScaleUI("x", 1);
            MoveWall(wallUI, "x", 0.5f);
        }
        else if(GetUIScale("x") > 1.6f)
        {
            MoveWall(wallZMax, "x", -1);

            MoveWall(wallXMin, "x", -0.5f);
            ScaleWall(wallXMin, "x", -1);

            MoveWall(wallXMax, "x", -0.5f);
            ScaleWall(wallXMax, "x", -1);

            //floor
            MoveWall(floorGB, "x", -0.5f);
            ScaleWall(floorGB, "x", -1);

            //ui
            ScaleUI("x", -1);
            MoveWall(wallUI, "x", -0.5f);
        }
    }
    #endregion
    #endregion

    #region ShowHideUI

    /// <summary>
    /// Turn on and off editing scaling mode of the wall
    /// </summary>
    /// <param name="editing"></param>
    public void EditingWall(bool editing)
    {
        if (editing)
        {
            if (!StaticClass.mainCamera.orthographic)
                GameObject.FindGameObjectWithTag("CAMERA").GetComponent<CameraController>().SwitchCamera();
            StaticClass.editingMode = StaticClass.EditingMode.Editing;
        }
        else
        {
            if (StaticClass.mainCamera.orthographic)
                GameObject.FindGameObjectWithTag("CAMERA").GetComponent<CameraController>().SwitchCamera();
            StaticClass.editingMode = StaticClass.EditingMode.Idle;
            ShowHideMoveScaleUI(editing);
        }

        if (!objectUI.activeSelf && !wallUI.activeSelf)
        {
            ShowHideMoveObjectUI(editing);
            objectUI.GetComponent<ObjectUIManager>().EditingObject(editing);
        }
        
    }

    /// <summary>
    /// Show and hide the wall UI
    /// </summary>
    /// <param name="show"></param>
    public void ShowHideMoveScaleUI(bool show)
    {
        wallUI.SetActive(show);
        if (show)
        {
            //Editing Mode
            wallUI.GetComponent<Canvas>().worldCamera = StaticClass.mainCamera;
        }
    }

    /// <summary>
    /// Show and hide Object UI
    /// </summary>
    /// <param name="show"></param>
    private void ShowHideMoveObjectUI(bool show)
    {
        objectUI.SetActive(show);
        if (show)
        {
            //Editing Mode
            objectUI.GetComponent<Canvas>().worldCamera = StaticClass.mainCamera;
        }
    }

    #endregion
}
