﻿using UnityEngine;

public class StaticClass : MonoBehaviour
{

    public static Camera mainCamera = Camera.main;

    public static GameObject moveGB = null;
    public static GameObject hitGB = null;
    public static PanMode panMode = PanMode.CameraMove;
    public static EditingMode editingMode = EditingMode.Idle;

    public enum PanMode
    {
        CameraMove,
        ObjectMoveFree,
        ObjectMoveGrid
    }

    public enum EditingMode
    {
        Editing,
        Idle
    }

    /// <summary>
    /// The global function to convert vector2 coordinate into world vector3 coordinate
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    public static Vector3 ScreenToWorld(Vector3 position)
    {
        if (panMode == PanMode.CameraMove)
            position.z = mainCamera.nearClipPlane;
        else if (panMode == PanMode.ObjectMoveFree || panMode == PanMode.ObjectMoveGrid)
        {
            position.z = moveGB.transform.position.z + mainCamera.nearClipPlane;
        }
        Vector3 worldPos = mainCamera.ScreenToWorldPoint(position);
        if (mainCamera.orthographic)
        {
            worldPos = new Vector3(worldPos.x, worldPos.z, worldPos.y);
        }
        return worldPos;
    }

    /// <summary>
    /// The function to snap the position of the gameobject to a grip
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    public static Vector3 SnapToGrid(Vector3 position)
    {
        return new Vector3(Mathf.RoundToInt(position.x), position.y, Mathf.RoundToInt(position.z));
    }

    /// <summary>
    /// The global function to rotate the object
    /// </summary>
    /// <param name="gameObject"></param>
    public static void RotateObject(GameObject gameObject)
    {
        gameObject.transform.Rotate(0, 90, 0);
    }

    /// <summary>
    /// The global function to generate random number
    /// </summary>
    /// <param name="minNum">min number inclusive</param>
    /// <param name="maxNum">max number inclusive</param>
    /// <returns></returns>
    public static float RandomNumberGenerator(float minNum, float maxNum)
    {
        return Random.Range(minNum, maxNum);
    }

}
