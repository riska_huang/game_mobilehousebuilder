﻿using System.Collections;
using UnityEngine;

public class PanDetection : MonoBehaviour
{
    private InputManager inputManager;

    private Vector2 startPosition;
    private float startTime;
    private Vector2 endPosition;
    private float endTime;

    private Coroutine cameraCoroutine;
    private Camera mainCamera;

    private void Awake()
    {
        inputManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<InputManager>();
    }

    private void OnEnable()
    {
        inputManager.OnStartPan += SwipeStart;
        inputManager.OnEndPan += SwipeEnd;
    }

    private void OnDisable()
    {
        inputManager.OnStartPan -= SwipeStart;
        inputManager.OnEndPan -= SwipeEnd;
    }
    private void SwipeStart(Vector2 position, float time)
    {
        startPosition = position;
        startTime = time;
        cameraCoroutine = StartCoroutine(cameraMovement());
    }

    private IEnumerator cameraMovement()
    {
        mainCamera = StaticClass.mainCamera;
        GameObject movingGB = StaticClass.moveGB;

        float speed = 40;
        if (mainCamera.orthographic)
            speed = 4;

        while (true)
        {
            Vector3 direction = inputManager.PrimaryPosition() - startPosition;
            if(StaticClass.panMode == StaticClass.PanMode.CameraMove)
            {
                mainCamera.transform.Translate(direction * Time.fixedDeltaTime * speed);
            }
            else //change with screenpointtoray methods
            {
                if (movingGB != null && direction.magnitude > 0.1f)
                {
                    if(movingGB.CompareTag("Wall") || movingGB.CompareTag("GameObject"))
                    {
                        direction = inputManager.PrimaryPosition();
                        direction = new Vector3(direction.x, direction.z, direction.y);
                        movingGB.transform.position = Vector3.MoveTowards(movingGB.transform.position, direction, Time.fixedDeltaTime * speed);
                    }
                }
            }
            startPosition = inputManager.PrimaryPosition();
            yield return null;
        }
    }

    private void SwipeEnd(Vector2 position, float time)
    {
        if(cameraCoroutine != null)
            StopCoroutine(cameraCoroutine);
        endPosition = position;
        endTime = time;
    }
}
