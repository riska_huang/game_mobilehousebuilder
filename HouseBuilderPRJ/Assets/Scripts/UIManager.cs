﻿using UnityEngine;

public class UIManager : MonoBehaviour
{
    private GameObject furnitureOptionGB;

    #region Walls
    [Header("Walls")]
    [SerializeField]
    private GameObject squareWallGB;
    #endregion

    #region Furniture
    [Header("Furniture")]
    [SerializeField]
    private GameObject chairGB;
    [SerializeField]
    private GameObject couchGB;
    [SerializeField]
    private GameObject bunkbedGB;
    [SerializeField]
    private GameObject cupboardGB;
    [SerializeField]
    private GameObject deskGB;
    [SerializeField]
    private GameObject tableGB;
    [SerializeField]
    private GameObject rugGB;
    #endregion

    private void Start()
    {
        furnitureOptionGB = transform.GetChild(1).gameObject;
    }

    #region Show Hide UI

    /// <summary>
    /// Hide all menu option
    /// </summary>
    public void HideAllOption()
    {
        furnitureOptionGB.SetActive(false);
    }

    /// <summary>
    /// Show specific menu option
    /// </summary>
    /// <param name="optionUIname"></param>
    public void ShowOptionUI(string optionUIname)
    {
        HideAllOption();
        switch (optionUIname)
        {
            case "furniture":
                furnitureOptionGB.SetActive(true);
                break;
        }
    }

    #endregion

    #region Placing Game Object
    /// <summary>
    /// Place game object into the scene
    /// </summary>
    /// <param name="gbName"></param>
    public void PlaceGameObject(string gbName)
    {
        GameObject summonedGB = null;
        switch (gbName)
        {
            case "SquareWall":
                summonedGB = Instantiate(squareWallGB);
                summonedGB.GetComponent<WallManager>().EditingWall(true);
                break;
            case "Chair":
                summonedGB = Instantiate(chairGB);
                break;
            case "Couch":
                summonedGB = Instantiate(couchGB);
                break;
            case "BunkBed":
                summonedGB = Instantiate(bunkbedGB);
                break;
            case "Cupboard":
                summonedGB = Instantiate(cupboardGB);
                break;
            case "Desk":
                summonedGB = Instantiate(deskGB);
                break;
            case "Table":
                summonedGB = Instantiate(tableGB);
                break;
            case "Rug":
                summonedGB = Instantiate(rugGB);
                break;
        }
        if(gbName != "SquareWall" && summonedGB != null)
        {
            summonedGB.transform.GetChild(0).gameObject.SetActive(true);
            summonedGB.GetComponentInChildren<ObjectUIManager>().EditingObject(true);
        }
        HideAllOption();
    }
    #endregion

    #region Exit

    /// <summary>
    /// Exit the application
    /// </summary>
    public void ExitApplication()
    {
        Application.Quit();
    }
    #endregion
}
