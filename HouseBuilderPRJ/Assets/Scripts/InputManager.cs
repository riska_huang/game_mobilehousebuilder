﻿using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

[DefaultExecutionOrder(-1)]
public class InputManager : MonoBehaviour
{
    #region Events
    public delegate void StartPan(Vector2 position, float time);
    public event StartPan OnStartPan;
    public delegate void EndPan(Vector2 position, float time);
    public event StartPan OnEndPan;
    #endregion

    #region Pinching Variables

    private Coroutine zoomCoroutine;
    private float zoomSpeed = 4f;

    #endregion

    #region Camera
    private CameraControlInput cameraControlInput;
    private Camera mainCamera;
    #endregion

    private void Awake()
    {
        cameraControlInput = new CameraControlInput();
    }

    private void OnEnable()
    {
        cameraControlInput.Enable();
    }

    private void OnDisable()
    {
        cameraControlInput.Disable();
    }

    private void Start()
    {
        //Panning
        cameraControlInput.Panning.PrimaryContact.started += ctx => StartTouchPrimary(ctx);
        cameraControlInput.Panning.PrimaryContact.canceled += ctx => EndTouchPrimary(ctx);

        //Pinching
        cameraControlInput.Pinching.SecondaryTouchContact.started += _ => PinchStart();
        cameraControlInput.Pinching.SecondaryTouchContact.canceled += _ => PinchEnd();
    }

    #region Panning
    private void StartTouchPrimary(InputAction.CallbackContext context)
    {
        mainCamera = StaticClass.mainCamera;

        if (OnStartPan != null)
        {
            Ray ray = mainCamera.ScreenPointToRay(cameraControlInput.Panning.PrimaryPosition.ReadValue<Vector2>());
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                
                if(hit.collider != null)
                {
                    if (hit.collider.tag == "GameObject")
                    {
                        DeactiveEditingMode();
                        hit.collider.transform.GetChild(0).gameObject.SetActive(true);
                        hit.collider.GetComponentInChildren<ObjectUIManager>().EditingObject(true);
                        StaticClass.hitGB = hit.collider.gameObject;
                    }
                    else if (hit.collider.tag == "Wall")
                    {
                        DeactiveEditingMode();
                        hit.collider.GetComponentInParent<WallManager>().EditingWall(true);
                        StaticClass.hitGB = hit.collider.gameObject;
                    }
                    else
                    {
                       OnStartPan(StaticClass.ScreenToWorld(cameraControlInput.Panning.PrimaryPosition.ReadValue<Vector2>()), (float)context.startTime);
                    }
                }
            }
        }
    }

    private void DeactiveEditingMode()
    {
        if(StaticClass.hitGB != null)
        {
            GameObject gb = StaticClass.hitGB;
            if(gb.tag == "GameObject")
            {
                if (gb.transform.GetChild(0).gameObject.activeSelf)
                {
                    gb.GetComponentInChildren<ObjectUIManager>().EditingObject(false);
                    gb.transform.GetChild(0).gameObject.SetActive(false);
                }
            }
            else if(gb.tag == "Wall")
            {
                gb.GetComponentInParent<WallManager>().EditingWall(true);
            }
        }
    }

    private void EndTouchPrimary(InputAction.CallbackContext context)
    {
        if (OnEndPan != null)
        {
            OnEndPan(StaticClass.ScreenToWorld(cameraControlInput.Panning.PrimaryPosition.ReadValue<Vector2>()), (float)context.time);
        }
    }

    public Vector2 PrimaryPosition()
    {
        mainCamera = StaticClass.mainCamera;

        return StaticClass.ScreenToWorld(cameraControlInput.Panning.PrimaryPosition.ReadValue<Vector2>());
    }

    #endregion

    #region Pinching

    private void PinchStart()
    {
        zoomCoroutine = StartCoroutine(ZoomDetection());
    }

    private void PinchEnd()
    {
        StopCoroutine(zoomCoroutine);
    }

    private IEnumerator ZoomDetection()
    {
        Transform cameraTransform = StaticClass.mainCamera.transform;
        float previousDistance = 0f, distance = 0f;
        while (true)
        {
            distance = Vector2.Distance(cameraControlInput.Pinching.PrimaryFingerPosition.ReadValue<Vector2>(), cameraControlInput.Pinching.SecondaryFingerPosition.ReadValue<Vector2>());

            //Target Position
            Vector3 targetPosition = cameraTransform.position;
            //Zoom out
            if (distance > previousDistance)
            {
                targetPosition.z -= 1;
            }
            //Zoom in
            else if(distance < previousDistance)
            {
                targetPosition.z += 1;
            }
            //Update camera
            cameraTransform.position = Vector3.Slerp(cameraTransform.position, targetPosition, Time.fixedDeltaTime * zoomSpeed);
            //Keep track of previous distance for next loop
            previousDistance = distance;
            yield return null;
        }
    }

    #endregion
}
