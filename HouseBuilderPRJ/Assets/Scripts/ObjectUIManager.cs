﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectUIManager : MonoBehaviour
{
    private GameObject objectUI, parentGB;

    private MaterialPropertyBlock propBlock;
    private Vector3 initPosition;
    private Quaternion initRotation;
    private Color initColor;

    public ObjectType objectType;
    public enum ObjectType
    {
        Wall,
        OtherObject
    }

    #region Unity Methods
    /// <summary>
    /// Initialize variable
    /// </summary>
    private void Awake()
    {
        objectUI = this.gameObject;
        parentGB = objectUI.transform.parent.gameObject;
        propBlock = new MaterialPropertyBlock();
    }

    private void FixedUpdate()
    {
        if (objectUI.activeSelf)
            AdjustCamera(); //only available on editing mode
    }

    #endregion

    #region Show Hide UI

    /// <summary>
    /// The UI always facing the camera
    /// Only active when the UI is active
    /// </summary>
    private void AdjustCamera()
    {
        objectUI.transform.LookAt(StaticClass.mainCamera.transform);
        if(objectUI.GetComponent<Canvas>().worldCamera != null)
        {
            if (objectUI.GetComponent<Canvas>().worldCamera.name != StaticClass.mainCamera.name)
                objectUI.GetComponent<Canvas>().worldCamera = StaticClass.mainCamera;
        }
        
    }

    /// <summary>
    /// Turn on and off editing mode of the gameobject
    /// </summary>
    /// <param name="editing"></param>
    public void EditingObject(bool editing)
    {
        if (editing)
        {
            StaticClass.editingMode = StaticClass.EditingMode.Editing;
            //Save the initial gameobject
            SaveInitialGameObject();
        }
        else if (!editing)
        {
            StaticClass.editingMode = StaticClass.EditingMode.Idle;
            objectUI.SetActive(false);
        }
    }

    #endregion

    #region Cancel Editing
    /// <summary>
    /// Save initial position, rotation, and color of the gameobject
    /// </summary>
    private void SaveInitialGameObject()
    {
        //save position
        initPosition = parentGB.transform.position;
        //save rotation
        initRotation = parentGB.transform.rotation;
        //save material color
        if(objectType == ObjectType.OtherObject)
        {
            initColor = parentGB.GetComponent<MeshRenderer>().material.GetColor("_Color");
        }
        else if(objectType == ObjectType.Wall)
        {
            initColor = parentGB.transform.GetChild(0).GetComponent<MeshRenderer>().material.GetColor("_Color");
        }
    }

    /// <summary>
    /// Return the gameobject to their initial state
    /// </summary>
    public void BackToInit()
    {
        parentGB.transform.position = initPosition;
        parentGB.transform.rotation = initRotation;
        if (objectType == ObjectType.OtherObject)
        {
            ChangeObjectColor(initColor, parentGB);
        }
        else if (objectType == ObjectType.Wall)
        {
            for (int i = 0; i < 4; i++)
            {
                ChangeObjectColor(initColor, parentGB.transform.GetChild(i).gameObject);
            }
        }
    }

    #endregion

    #region Change Color
    /// <summary>
    /// Change the color of only one gameobject
    /// </summary>
    /// <param name="newColor"></param>
    /// <param name="appliedGB"></param>
    private void ChangeObjectColor(Color newColor, GameObject appliedGB)
    {
        appliedGB.GetComponent<MeshRenderer>().GetPropertyBlock(propBlock);
        propBlock.SetColor("_Color", newColor);

        appliedGB.GetComponent<MeshRenderer>().SetPropertyBlock(propBlock);
    }

    /// <summary>
    /// Generate and change the color of the gameobject 
    /// </summary>
    public void ChangeRandomColor()
    {
        Color color = new Color(StaticClass.RandomNumberGenerator(0.0f, 1.0f), StaticClass.RandomNumberGenerator(0.0f, 1.0f), StaticClass.RandomNumberGenerator(0.0f, 1.0f));
        if(objectType == ObjectType.OtherObject)
        {
            ChangeObjectColor(color,parentGB);
        }
        else if(objectType == ObjectType.Wall)
        {
            for(int i = 0; i< 4; i++)
            {
                ChangeObjectColor(color, parentGB.transform.GetChild(i).gameObject);
            }
        }
    }

    #endregion

    #region Delete Object

    /// <summary>
    /// Delete the summoned gameobject
    /// </summary>
    public void DeleteGameObject()
    {
        Destroy(parentGB);
    }

    #endregion

    #region Moving Object
    /// <summary>
    /// Switch the object UI into scale wall UI
    /// </summary>
    public void ChangeWallEditUI()
    {
        parentGB.GetComponent<WallManager>().ShowHideMoveScaleUI(true);
        objectUI.SetActive(false);
    }

    /// <summary>
    /// Move the object when the user begin panning
    /// </summary>
    /// <param name="beginMove"></param>
    public void BeginEndMoveObject(bool beginMove) //merge with the moving wall
    {   
        if (beginMove)
        {
            StaticClass.moveGB = parentGB;
            if (objectType == ObjectType.OtherObject)
                StaticClass.panMode = StaticClass.PanMode.ObjectMoveFree;
            else if (objectType == ObjectType.Wall)
                StaticClass.panMode = StaticClass.PanMode.ObjectMoveGrid;
            
        }
        else if (!beginMove)
        {
            StaticClass.panMode = StaticClass.PanMode.CameraMove;
            if(objectType == ObjectType.Wall)
                parentGB.transform.position = StaticClass.SnapToGrid(parentGB.transform.position);
        }
    }

    #endregion

    #region Rotate

    /// <summary>
    /// Rotate the gameobject
    /// </summary>
    public void RotateGameObject()
    {
        StaticClass.RotateObject(parentGB);
    }

    #endregion
}
