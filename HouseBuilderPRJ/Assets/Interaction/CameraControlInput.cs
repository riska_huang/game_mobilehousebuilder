// GENERATED AUTOMATICALLY FROM 'Assets/Interaction/CameraControlInput.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @CameraControlInput : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @CameraControlInput()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""CameraControlInput"",
    ""maps"": [
        {
            ""name"": ""Panning"",
            ""id"": ""baf2ebe8-1f99-4745-927f-ec41c58815b3"",
            ""actions"": [
                {
                    ""name"": ""PrimaryContact"",
                    ""type"": ""PassThrough"",
                    ""id"": ""aa0e9a84-e6a1-4452-8524-e7679762b7dd"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""PrimaryPosition"",
                    ""type"": ""PassThrough"",
                    ""id"": ""0692d7d9-6977-47aa-9026-c460fc73ae87"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""0d265283-4cd2-4b94-9ad8-ebcd26672ff4"",
                    ""path"": ""<Touchscreen>/primaryTouch/press"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PrimaryContact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6a1d9c0c-50eb-438c-b370-4ac500206fdb"",
                    ""path"": ""<Touchscreen>/primaryTouch/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PrimaryPosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Pinching"",
            ""id"": ""471c8839-1cd4-404a-8f12-aa027c5cd45b"",
            ""actions"": [
                {
                    ""name"": ""PrimaryFingerPosition"",
                    ""type"": ""Value"",
                    ""id"": ""f81f56e9-bc12-4182-a1a2-6f472681c917"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SecondaryFingerPosition"",
                    ""type"": ""Value"",
                    ""id"": ""642c74c4-39e5-426c-8013-aefcd6426d03"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SecondaryTouchContact"",
                    ""type"": ""Button"",
                    ""id"": ""00c38bf0-5869-442d-bb17-4de2043eb0eb"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""124254c1-b4f4-4051-a9ad-a7d707c08c4a"",
                    ""path"": ""<Touchscreen>/touch0/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PrimaryFingerPosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f5ccbaa3-2027-4185-87f2-785f8003731f"",
                    ""path"": ""<Touchscreen>/touch1/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SecondaryFingerPosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""67d25e09-6b63-4fff-ba21-d9a86e387c65"",
                    ""path"": ""<Touchscreen>/touch1/press"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SecondaryTouchContact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Panning
        m_Panning = asset.FindActionMap("Panning", throwIfNotFound: true);
        m_Panning_PrimaryContact = m_Panning.FindAction("PrimaryContact", throwIfNotFound: true);
        m_Panning_PrimaryPosition = m_Panning.FindAction("PrimaryPosition", throwIfNotFound: true);
        // Pinching
        m_Pinching = asset.FindActionMap("Pinching", throwIfNotFound: true);
        m_Pinching_PrimaryFingerPosition = m_Pinching.FindAction("PrimaryFingerPosition", throwIfNotFound: true);
        m_Pinching_SecondaryFingerPosition = m_Pinching.FindAction("SecondaryFingerPosition", throwIfNotFound: true);
        m_Pinching_SecondaryTouchContact = m_Pinching.FindAction("SecondaryTouchContact", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Panning
    private readonly InputActionMap m_Panning;
    private IPanningActions m_PanningActionsCallbackInterface;
    private readonly InputAction m_Panning_PrimaryContact;
    private readonly InputAction m_Panning_PrimaryPosition;
    public struct PanningActions
    {
        private @CameraControlInput m_Wrapper;
        public PanningActions(@CameraControlInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @PrimaryContact => m_Wrapper.m_Panning_PrimaryContact;
        public InputAction @PrimaryPosition => m_Wrapper.m_Panning_PrimaryPosition;
        public InputActionMap Get() { return m_Wrapper.m_Panning; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PanningActions set) { return set.Get(); }
        public void SetCallbacks(IPanningActions instance)
        {
            if (m_Wrapper.m_PanningActionsCallbackInterface != null)
            {
                @PrimaryContact.started -= m_Wrapper.m_PanningActionsCallbackInterface.OnPrimaryContact;
                @PrimaryContact.performed -= m_Wrapper.m_PanningActionsCallbackInterface.OnPrimaryContact;
                @PrimaryContact.canceled -= m_Wrapper.m_PanningActionsCallbackInterface.OnPrimaryContact;
                @PrimaryPosition.started -= m_Wrapper.m_PanningActionsCallbackInterface.OnPrimaryPosition;
                @PrimaryPosition.performed -= m_Wrapper.m_PanningActionsCallbackInterface.OnPrimaryPosition;
                @PrimaryPosition.canceled -= m_Wrapper.m_PanningActionsCallbackInterface.OnPrimaryPosition;
            }
            m_Wrapper.m_PanningActionsCallbackInterface = instance;
            if (instance != null)
            {
                @PrimaryContact.started += instance.OnPrimaryContact;
                @PrimaryContact.performed += instance.OnPrimaryContact;
                @PrimaryContact.canceled += instance.OnPrimaryContact;
                @PrimaryPosition.started += instance.OnPrimaryPosition;
                @PrimaryPosition.performed += instance.OnPrimaryPosition;
                @PrimaryPosition.canceled += instance.OnPrimaryPosition;
            }
        }
    }
    public PanningActions @Panning => new PanningActions(this);

    // Pinching
    private readonly InputActionMap m_Pinching;
    private IPinchingActions m_PinchingActionsCallbackInterface;
    private readonly InputAction m_Pinching_PrimaryFingerPosition;
    private readonly InputAction m_Pinching_SecondaryFingerPosition;
    private readonly InputAction m_Pinching_SecondaryTouchContact;
    public struct PinchingActions
    {
        private @CameraControlInput m_Wrapper;
        public PinchingActions(@CameraControlInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @PrimaryFingerPosition => m_Wrapper.m_Pinching_PrimaryFingerPosition;
        public InputAction @SecondaryFingerPosition => m_Wrapper.m_Pinching_SecondaryFingerPosition;
        public InputAction @SecondaryTouchContact => m_Wrapper.m_Pinching_SecondaryTouchContact;
        public InputActionMap Get() { return m_Wrapper.m_Pinching; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PinchingActions set) { return set.Get(); }
        public void SetCallbacks(IPinchingActions instance)
        {
            if (m_Wrapper.m_PinchingActionsCallbackInterface != null)
            {
                @PrimaryFingerPosition.started -= m_Wrapper.m_PinchingActionsCallbackInterface.OnPrimaryFingerPosition;
                @PrimaryFingerPosition.performed -= m_Wrapper.m_PinchingActionsCallbackInterface.OnPrimaryFingerPosition;
                @PrimaryFingerPosition.canceled -= m_Wrapper.m_PinchingActionsCallbackInterface.OnPrimaryFingerPosition;
                @SecondaryFingerPosition.started -= m_Wrapper.m_PinchingActionsCallbackInterface.OnSecondaryFingerPosition;
                @SecondaryFingerPosition.performed -= m_Wrapper.m_PinchingActionsCallbackInterface.OnSecondaryFingerPosition;
                @SecondaryFingerPosition.canceled -= m_Wrapper.m_PinchingActionsCallbackInterface.OnSecondaryFingerPosition;
                @SecondaryTouchContact.started -= m_Wrapper.m_PinchingActionsCallbackInterface.OnSecondaryTouchContact;
                @SecondaryTouchContact.performed -= m_Wrapper.m_PinchingActionsCallbackInterface.OnSecondaryTouchContact;
                @SecondaryTouchContact.canceled -= m_Wrapper.m_PinchingActionsCallbackInterface.OnSecondaryTouchContact;
            }
            m_Wrapper.m_PinchingActionsCallbackInterface = instance;
            if (instance != null)
            {
                @PrimaryFingerPosition.started += instance.OnPrimaryFingerPosition;
                @PrimaryFingerPosition.performed += instance.OnPrimaryFingerPosition;
                @PrimaryFingerPosition.canceled += instance.OnPrimaryFingerPosition;
                @SecondaryFingerPosition.started += instance.OnSecondaryFingerPosition;
                @SecondaryFingerPosition.performed += instance.OnSecondaryFingerPosition;
                @SecondaryFingerPosition.canceled += instance.OnSecondaryFingerPosition;
                @SecondaryTouchContact.started += instance.OnSecondaryTouchContact;
                @SecondaryTouchContact.performed += instance.OnSecondaryTouchContact;
                @SecondaryTouchContact.canceled += instance.OnSecondaryTouchContact;
            }
        }
    }
    public PinchingActions @Pinching => new PinchingActions(this);
    public interface IPanningActions
    {
        void OnPrimaryContact(InputAction.CallbackContext context);
        void OnPrimaryPosition(InputAction.CallbackContext context);
    }
    public interface IPinchingActions
    {
        void OnPrimaryFingerPosition(InputAction.CallbackContext context);
        void OnSecondaryFingerPosition(InputAction.CallbackContext context);
        void OnSecondaryTouchContact(InputAction.CallbackContext context);
    }
}
